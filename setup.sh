#!/bin/bash

#to be able to use homebrew on Mac (https://www.datacamp.com/community/tutorials/homebrew-install-use)
xcode-select --install

#Install homebrew
brew

#Install docker
brew install docker

#Install heroku cli
brew tap heroku/brew && brew install heroku

#Install yarn (might also install nodeV16 but we want nodeV12 so will install nodeV12 seperately)
brew install yarn --ignore-dependencies

#Install nvm
brew install nvm

mkdir ~/.nvm

grep -q 'export PATH="/Users/$USER/.bash_profile"' /Users/$USER/.bash_profile || echo 'export PATH="/Users/$USER/.bash_profile"' >> /Users/$USER/.bash_profile

grep -q '[ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh" # This loads nvm' /Users/$USER/.bash_profile || echo '[ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh" # This loads nvm' >> /Users/$USER/.bash_profile

grep -q '[ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && . "/usr/local/opt/nvm/etc/bash_completion.d/nvm" # This loads nvm bash_completion' /Users/$USER/.bash_profile || echo '[ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && . "/usr/local/opt/nvm/etc/bash_completion.d/nvm" # This loads nvm bash_completion' >> /Users/$USER/.bash_profile

#Install node@12
source $(brew --prefix nvm)/nvm.sh
nvm install --lts=Erbium

#Install redis
brew install redis

#Install postgresql (for v11)
brew install postgresql@11

#install kotlin (should come come with  openjdk 15.0.2)
brew install kotlin

#install maven
brew install maven

